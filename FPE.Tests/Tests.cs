using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace FPE.Tests;

public class Tests
{
	[Theory]
	[MemberData(nameof(TestVectors))]
	public void Test(TestVector vec)
	{
		using AesCipher aes = new AesCipher(vec.Key);
		FF1 ff1 = new FF1(aes, vec.Radix);

		{
			INumeralString plain = new NumeralString16(vec.Radix, vec.PlainText);
			INumeralString cipher = ff1.Encrypt(plain, vec.Tweak);
			Assert.True(cipher.Select(x => (ushort)x).SequenceEqual(vec.CipherText), "Encryption");
		}

		{
			INumeralString cipher = new NumeralString16(vec.Radix, vec.CipherText);
			INumeralString plain = ff1.Decrypt(cipher, vec.Tweak);
			Assert.True(plain.Select(x => (ushort)x).SequenceEqual(vec.PlainText), "Decryption");
		}
	}

	[Fact]
	public void Sample()
	{
		// Key must be 128, 192 or 256 bits (i.e. 16, 24 or 32 bytes).
		var key = Encoding.UTF8.GetBytes("Key used for AES");

		// Example: encrypting a credit card number.
		int[] creditCardNumber = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5 };

		// Create a base 10 numeral string.
		var plaintext = NumeralString.Create(10, creditCardNumber);

		// Set up AES and FF1.
		using var aes = new AesCipher(key);
		var ff1 = new FF1(aes, 10);

		// Encrypt.
		var encrypted = ff1.Encrypt(plaintext);

		// The result is a new 12-digit base 10 string that looks just like a credit card number
		// but is in fact encrypted using AES and the key specified above. 
		Assert.Equal(new[] { 0, 1, 3, 1, 2, 0, 0, 7, 9, 4, 9, 4, 1, 1, 7, 7 }, encrypted);

		// Decrypting it yields the original.
		var decrypted = ff1.Decrypt(encrypted);

		Assert.Equal(plaintext, decrypted);
	}

	private static void testRoundtrip(FF1 ff1, ushort[] data)
	{
		NumeralString16 str = new NumeralString16(ff1.Radix, data);

		INumeralString encrypted = ff1.Encrypt(str, ReadOnlySpan<byte>.Empty);
		INumeralString decrypted = ff1.Decrypt(encrypted, ReadOnlySpan<byte>.Empty);

		Assert.True(decrypted.Select(x => (ushort)x).SequenceEqual(data));
	}

	[Fact]
	public void MaxRadix()
	{
		ushort[] data = { 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF };

		AesCipher aes = new AesCipher(Enumerable.Range(1, 32).Select(x => (byte)x).ToArray());
		FF1 ff1 = new FF1(aes, 65536);
		testRoundtrip(ff1, data);
	}

	[Theory]
	[MemberData(nameof(RandomSeeds))]
	public void RandomRoundtrip(int seed, int keyLen)
	{
		Random rng = new Random(seed);

		int radix = rng.Next(2, 65536);
		int len = rng.Next(1, 100);

		ushort[] data = new ushort[len];
		for (int i = 0; i < len; ++i)
		{
			data[i] = (ushort)rng.Next(0, radix);
		}

		int keySize = keyLen switch
		{
			128 => 16,
			192 => 24,
			256 => 32,
			_ => throw new ArgumentOutOfRangeException(nameof(keyLen), keyLen, null)
		};

		byte[] key = new byte[keySize];
		rng.NextBytes(key); // Beware, not cryptographically safe!

		AesCipher aes = new AesCipher(key);
		FF1 ff1 = new FF1(aes, radix);
		testRoundtrip(ff1, data);
	}

	[Fact]
	public void NonStandardRounds()
	{
		Random rng = new Random(1337);
		byte[] key = new byte[32];
		rng.NextBytes(key); // Beware, not cryptographically safe!

		AesCipher aes = new AesCipher(key);
		FF1 ff1 = new FF1(aes, 10);

		for (int i = 0; i < 20; ++i)
		{
			ff1.Rounds = i;
			testRoundtrip(ff1, new ushort[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
		}
	}

	public static IEnumerable<object[]> TestVectors => TestVector.All.Select(v => new object[] { v });

	public static IEnumerable<object[]> RandomSeeds => Enumerable.Range(1, 50).SelectMany(randomRoundtripParams);

	private static IEnumerable<object[]> randomRoundtripParams(int seed)
	{
		yield return new object[] { seed, 128 };
		yield return new object[] { seed, 192 };
		yield return new object[] { seed, 256 };
	}
}