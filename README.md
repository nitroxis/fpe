FF1 format-preserving encryption
============================

This library implements the format-preserving encryption algorithm FF1 as defined in [NIST SP 800-38G](https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-38G.pdf). It can be used to encrypt information while preserving its format, i.e. encrypting a 5-digit number will yield another 5-digit number, encrypting a Base64 string will yield another Base64 string of the same length etc.

This is useful for e.g. storing information in databases that do not support encryption and except a specific data type or format.

Usage
-----

```c#
// Key must be 128, 192 or 256 bits (i.e. 16, 24 or 32 bytes).
var key = Encoding.UTF8.GetBytes("Key used for AES");

// Example: encrypting a credit card number.
int[] creditCardNumber = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5 };

// Create a base 10 numeral string.
var plaintext = NumeralString.Create(10, creditCardNumber);

// Set up AES and FF1.
using var aes = new AesCipher(key);
var ff1 = new FF1(aes, 10);

// Encrypt.
var encrypted = ff1.Encrypt(plaintext);

// The result is a new 12-digit base 10 string that looks just like a credit card number
// but is in fact encrypted using AES and the key specified above. 
Assert.Equal(new[] { 0, 1, 3, 1, 2, 0, 0, 7, 9, 4, 9, 4, 1, 1, 7, 7 }, encrypted);

// Decrypting it yields the original.
var decrypted = ff1.Decrypt(encrypted);

Assert.Equal(plaintext, decrypted);
```

"Tweaks" as specified in the NIST publication are also supported. It is also in theory possible to plug in other `ICipher` implementations, the only requirement is that the cipher must be stateless (i.e. no block chaining) and that it works with a block size of 128 bits.

License
-------
MIT
