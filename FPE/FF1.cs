﻿using System.Buffers.Binary;
using System.Numerics;

namespace FPE;

/// <summary>
/// Implements the FF1 format-preserving encryption algorithm.
/// </summary>
public sealed class FF1
{
	#region Fields

	internal const int BlockSize = 16;

	private readonly ICipher cipher;

	private readonly int log2Radix;
	private readonly double log2RadixFloat;

	#endregion

	#region Properties

	/// <summary>
	/// Gets the radix used by this <see cref="FF1"/> instance.
	/// </summary>
	public int Radix { get; }

	/// <summary>
	/// Gets or sets the number of Feistel rounds to use.
	/// </summary>
	/// <remarks>
	/// The default for FF1 is defined to be 10 rounds.
	/// </remarks>
	public int Rounds { get; set; } = 10;

	#endregion

	#region Constructors

	/// <summary>
	/// Creates a new <see cref="FF1"/> instance for the specified <see cref="ICipher">cipher</see> and radix.
	/// </summary>
	/// <param name="cipher"></param>
	/// <param name="radix"></param>
	/// <exception cref="ArgumentException"></exception>
	public FF1(ICipher cipher, int radix)
	{
		if (radix <= 1)
			throw new ArgumentException("Radix must be greater than 1", nameof(radix));

		this.Radix = radix;
		this.cipher = cipher;

		if (BitOperations.PopCount((uint)radix) == 1)
		{
			// Power of two, use integer math.
			this.log2Radix = BitOperations.Log2((uint)radix);
			this.log2RadixFloat = double.NaN;
		}
		else
		{
			// Not power of two, fallback to floating point math.
			this.log2Radix = -1;
			this.log2RadixFloat = Math.Log2(radix);
		}
	}

	#endregion

	#region Methods

	private static int write(ReadOnlySpan<byte> from, Span<byte> to)
	{
		if (from.Length > to.Length)
		{
			from[..to.Length].CopyTo(to);
			return to.Length;
		}

		from.CopyTo(to);
		return Math.Min(to.Length, from.Length);
	}

	/// <summary>
	/// Construct S as defined in the FF1 NIST paper.
	/// </summary>
	private void buildS(Span<byte> r, Span<byte> s)
	{
		int max = (s.Length + BlockSize - 1) / BlockSize;

		s = s[write(r, s)..];
		if (s.Length == 0)
			return;

		Span<byte> temp = stackalloc byte[r.Length];
		for (uint i = 1; i < max; ++i)
		{
			r.CopyTo(temp);

			Span<byte> xor = temp[^4..];
			xor[0] ^= (byte)(i >> 24);
			xor[1] ^= (byte)(i >> 16);
			xor[2] ^= (byte)(i >> 8);
			xor[3] ^= (byte)(i >> 0);
			this.cipher.Transform(temp);

			s = s[write(temp, s)..];
			if (s.Length == 0)
				break;
		}
	}

	/// <summary>
	/// Encode the specified <see cref="BigInteger"/> into big-endian bytes.
	/// </summary>
	private static void getBigEndianBytes(BigInteger x, Span<byte> dest)
	{
		// There is a big endian parameter for TryWriteBytes, however it seems to add an additional zero byte at the end which is not good.
		// This method is probably a little slower but it works reliably.
		dest.Clear();
		x.TryWriteBytes(dest, out _, true);
		dest.Reverse();
	}

	/// <summary>
	/// Compute B as defined in the FF1 NIST paper.
	/// </summary>
	private int calculateB(int v)
	{
		if (this.log2Radix < 0)
		{
			return (int)Math.Ceiling(v * this.log2RadixFloat / 8.0);
		}

		return ((v * this.log2Radix) + 7) / 8;
	}

	private INumeralString str(BigInteger value, int m)
	{
		if (this.Radix <= 256)
		{
			return NumeralString8.FromBigInteger(value, this.Radix, m);
		}

		return NumeralString16.FromBigInteger(value, this.Radix, m);
	}

	/// <summary>
	/// Transforms the specified input string with an optional tweak in the specified direction (encryption or decryption).
	/// </summary>
	/// <param name="x">The input numeral string.</param>
	/// <param name="encrypt">Whether to encrypt (true) or decrypt (false).</param>
	/// <param name="tweak">An optional tweak byte-string.</param>
	/// <returns>The encrypted/decrypted numeral string.</returns>
	/// <exception cref="ArgumentException"></exception>
	public INumeralString Transform(INumeralString x, bool encrypt, ReadOnlySpan<byte> tweak = default)
	{
		int n = x.Count;
		int t = tweak.Length;

		// 1. Let u = floor(n / 2); v = n - u
		int u = n / 2;
		int v = n - u;

		// 2. Let A = X[1..u]; B = X[u + 1..n].
		(INumeralString A, INumeralString B) = x.Split(u);

		// 3. Let b = ceil(ceil(v * log2(radix)) / 8).
		int b = this.calculateB(v);

		// 4. Let d = 4 * ceil(b / 4) + 4.
		int d = 4 * ((b + 3) / 4) + 4;

		// 5. Let P = [1, 2, 1] || [radix] || [10] || [u mod 256] || [n] || [t].
		Span<byte> p = stackalloc byte[BlockSize]
		{
			1, 2, 1, 0, 0, 0, 10, (byte)u, 0, 0, 0, 0, 0, 0, 0, 0
		};

		p[3] = (byte)(this.Radix >> 16);
		p[4] = (byte)(this.Radix >> 8);
		p[5] = (byte)(this.Radix >> 0);

		BinaryPrimitives.WriteUInt32BigEndian(p[8..12], (uint)n);
		BinaryPrimitives.WriteUInt32BigEndian(p[12..16], (uint)t);

		//  6i. Let Q = T || [0]^((-t-b-1) mod 16) || [i] || [NUM(B, radix)].
		// 6ii. Let R = PRF(P || Q).
		Prf prf = new Prf(this.cipher);
		prf.Update(p);
		prf.Update(tweak);

		int max = ((-t - b - 1) % BlockSize + BlockSize) % BlockSize;

		Span<byte> bigIntBytes = stackalloc byte[b];
		Span<byte> temp = stackalloc byte[1];
		Span<byte> r = stackalloc byte[BlockSize];
		Span<byte> s = stackalloc byte[d];

		for (int i = 0; i < max; ++i)
		{
			prf.Update(temp);
		}

		// Precompute these to speed things up a little.
		BigInteger radixBig = new BigInteger(this.Radix);
		BigInteger modulusU = BigInteger.Pow(radixBig, u);
		BigInteger modulusV = BigInteger.Pow(radixBig, v);

		Prf prfRound = new Prf(this.cipher);

		for (int round = 0; round < this.Rounds; ++round)
		{
			int i = encrypt ? round : this.Rounds - 1 - round;

			temp[0] = (byte)i;

			prfRound.CopyFrom(prf);
			prfRound.Update(temp);

			BigInteger num = encrypt ? B.ToBigInteger() : A.ToBigInteger();
			getBigEndianBytes(num, bigIntBytes);

			prfRound.Update(bigIntBytes);
			prfRound.CopyTo(r);

			// 6iii. Let S be the first d bytes of R.
			this.buildS(r, s);

			// 6iv. Let y = NUM(S).
			BigInteger y = new BigInteger(s, true, true);

			// 6v. If i is even, let m = u; else, let m = v.
			int m;
			BigInteger modulus;
			if (i % 2 == 0)
			{
				m = u;
				modulus = modulusU;
			}
			else
			{
				m = v;
				modulus = modulusV;
			}

			// 6vi. Let c = (NUM(A, radix) + y) mod radix^m.
			BigInteger c = encrypt ? A.ToBigInteger() + y : B.ToBigInteger() - y;

			c %= modulus;

			if (c.Sign < 0)
			{
				c += modulus;
				c %= modulus;
			}

			// 6vii. Let C = STR(c, radix).
			INumeralString C = this.str(c, m);

			if (encrypt)
			{
				// 6viii. Let A = B.
				// 6ix. Let B = C.
				(A, B) = (B, C);
			}
			else
			{
				// 6viii. Let B = A.
				// 6ix. Let A = C.
				(B, A) = (A, C);
			}
		}

		// 7. Return A || B.
		return NumeralString.Concat(this.Radix, A, B);
	}

	/// <summary>
	/// Encrypts the specified input numeral string with an optional tweak byte-string.
	/// </summary>
	/// <param name="x">The input numeral string.</param>
	/// <param name="tweak">An optional tweak byte-string.</param>
	/// <returns>The encrypted numeral string.</returns>
	public INumeralString Encrypt(INumeralString x, ReadOnlySpan<byte> tweak = default)
	{
		return this.Transform(x, true, tweak);
	}

	/// <summary>
	/// Decrypts the specified input numeral string with an optional tweak byte-string.
	/// </summary>
	/// <param name="x">The input numeral string.</param>
	/// <param name="tweak">An optional tweak byte-string.</param>
	/// <returns>The decrypted numeral string.</returns>
	public INumeralString Decrypt(INumeralString x, ReadOnlySpan<byte> tweak = default)
	{
		return this.Transform(x, false, tweak);
	}

	#endregion
}