using System.Security.Cryptography;

namespace FPE;

/// <summary>
/// Implements the <see cref="ICipher"/> interface for AES.
/// </summary>
public class AesCipher : ICipher, IDisposable
{
	private readonly Aes aes;

	public int BytesPerBlock => 16;

	private static readonly byte[] zeroIv = new byte[16];

	public AesCipher(ReadOnlySpan<byte> key)
	{
		this.aes = Aes.Create();
		this.aes.KeySize = key.Length switch
		{
			16 => 128,
			24 => 192,
			32 => 256,
			_ => throw new ArgumentException("Key must be either 128, 192 or 256 bit in length", nameof(key))
		};
		this.aes.Key = key.ToArray();
		this.aes.IV = zeroIv;
	}

	public void Transform(Span<byte> block)
	{
		if (block.Length != this.BytesPerBlock)
			throw new ArgumentException($"Invalid block length, must be {this.BytesPerBlock}", nameof(block));

		this.aes.EncryptEcb(block, block, PaddingMode.None);
	}

	public void Dispose()
	{
		this.aes.Dispose();
	}
}