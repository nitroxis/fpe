﻿using System.Text;

namespace FPE;

public static class NumeralString
{
	/// <summary>
	/// Create a numeral string for the specified values and radix.
	/// </summary>
	/// <param name="radix">The radix. Must be in the range 2..65536.</param>
	/// <param name="values">The values of the string.</param>
	/// <returns>A numeral string for the specified values.</returns>
	/// <exception cref="ArgumentOutOfRangeException"></exception>
	public static INumeralString Create(int radix, ReadOnlySpan<int> values)
	{
		if (radix <= 256)
		{
			byte[] data = new byte[values.Length];
			for (int i = 0; i < values.Length; ++i)
				data[i] = (byte)values[i];
			return new NumeralString8(radix, data);
		}

		if (radix <= 65536)
		{
			ushort[] data = new ushort[values.Length];
			for (int i = 0; i < values.Length; ++i)
				data[i] = (ushort)values[i];
			return new NumeralString16(radix, data);
		}

		throw new ArgumentOutOfRangeException(nameof(radix), "Radix must be less than or equal to 65536.");
	}

	/// <summary>
	/// Create a numeral string for the specified values and radix.
	/// </summary>
	/// <param name="radix">The radix. Must be in the range 2..65536.</param>
	/// <param name="values">The values of the string.</param>
	/// <returns>A numeral string for the specified values.</returns>
	/// <exception cref="ArgumentOutOfRangeException"></exception>
	public static INumeralString Create(int radix, params int[] values) => Create(radix, new ReadOnlySpan<int>(values));

	/// <summary>
	/// Concatenates two numeral strings.
	/// </summary>
	/// <param name="radix">The radix of the two strings.</param>
	/// <param name="a">The first string.</param>
	/// <param name="b">The second string.</param>
	/// <returns>The concatenated string A || B.</returns>
	public static INumeralString Concat(int radix, INumeralString a, INumeralString b)
	{
		if (radix <= 256)
		{
			byte[] data = new byte[a.Count + b.Count];
			for (int i = 0; i < a.Count; ++i)
				data[i] = (byte)a[i];
			for (int i = 0; i < b.Count; ++i)
				data[a.Count + i] = (byte)b[i];
			return new NumeralString8(radix, data);
		}
		else
		{
			ushort[] data = new ushort[a.Count + b.Count];
			for (int i = 0; i < a.Count; ++i)
				data[i] = (ushort)a[i];
			for (int i = 0; i < b.Count; ++i)
				data[a.Count + i] = (ushort)b[i];
			return new NumeralString16(radix, data);
		}
	}

	/// <summary>
	/// Returns a debug string for the specified <see cref="INumeralString"/>.
	/// </summary>
	/// <param name="numeralString"></param>
	/// <param name="maxLength"></param>
	/// <returns></returns>
	internal static string GetDebugString(INumeralString numeralString, int maxLength = 100)
	{
		StringBuilder str = new(100);

		foreach (int val in numeralString)
		{
			if (str.Length > 0)
			{
				str.Append(", ");
			}

			if (str.Length > maxLength)
			{
				str.Append("...");
				break;
			}

			str.Append(val);
		}

		return str.ToString();
	}
}