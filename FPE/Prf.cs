using System.Diagnostics;

namespace FPE;

/// <summary>
/// Implements the PRF for FF1.
/// </summary>
/// <remarks>
/// Corresponds to PRF in the NIST paper.
/// </remarks>
internal struct Prf
{
	private int offset;
	private readonly Cbc state;
	private readonly byte[] buffer;

	public Prf(ICipher cipher)
	{
		this.offset = 0;
		this.state = new Cbc(cipher);
		this.buffer = new byte[FF1.BlockSize];
	}

	/// <summary>
	/// Copy state from another <see cref="Prf"/> instance.
	/// Avoids allocations.
	/// </summary>
	/// <param name="other"></param>
	public void CopyFrom(Prf other)
	{
		Debug.Assert(other.buffer.Length == this.buffer.Length);

		this.offset = other.offset;
		this.state.CopyFrom(other.state);
		new Span<byte>(other.buffer).CopyTo(this.buffer);
	}

	public void Update(ReadOnlySpan<byte> data)
	{
		while (data.Length > 0)
		{
			// Append bytes from input data.
			int n = Math.Min(data.Length, this.buffer.Length - this.offset);
			data[..n].CopyTo(new Span<byte>(this.buffer, this.offset, n));
			data = data[n..];

			this.offset += n;
			if (this.offset == this.buffer.Length)
			{
				// One full block written, transform it.
				this.state.Transform(this.buffer);
				this.offset = 0;
			}
		}
	}

	public void CopyTo(Span<byte> dest)
	{
		if (this.offset != 0)
			throw new InvalidOperationException();

		((Span<byte>)this.buffer).CopyTo(dest);
	}
}