﻿namespace FPE;

/// <summary>
/// Abstract block cipher.
/// </summary>
public interface ICipher
{
	/// <summary>
	/// Transforms the specified block.
	/// </summary>
	/// <param name="block">The block to tranform (in place).</param>
	/// <remarks>Corresponds to CIPH_k in the NIST paper.</remarks>
	void Transform(Span<byte> block);
}