using System.Diagnostics;

namespace FPE;

/// <summary>
/// Implements cipher block chaining for a cipher.
/// </summary>
internal readonly struct Cbc : ICipher
{
	private readonly ICipher cipher;
	private readonly byte[] prevBlock;

	public int BytesPerBlock => FF1.BlockSize;

	public Cbc(ICipher cipher)
	{
		this.cipher = cipher;
		this.prevBlock = new byte[FF1.BlockSize];
	}

	/// <summary>
	/// Copy state from another <see cref="Cbc"/> instance.
	/// Avoids allocations.
	/// </summary>
	/// <param name="other"></param>
	public void CopyFrom(in Cbc other)
	{
		Debug.Assert(other.prevBlock.Length == this.prevBlock.Length);

		new Span<byte>(other.prevBlock).CopyTo(this.prevBlock);
	}

	public void Transform(Span<byte> block)
	{
		Debug.Assert(block.Length == this.prevBlock.Length);

		for (int i = 0; i < this.prevBlock.Length; ++i)
		{
			this.prevBlock[i] ^= block[i];
		}

		this.cipher.Transform(this.prevBlock);
		this.prevBlock.CopyTo(block);
	}
}