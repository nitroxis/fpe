using System.Collections;
using System.Diagnostics;
using System.Numerics;
using System.Runtime.InteropServices;

namespace FPE;

/// <summary>
/// Numeral string backed by a 16-bit integer array.
/// </summary>
/// <remarks>
/// Maximum radix is 65536.
/// </remarks>
[DebuggerDisplay("{DebugDisplay}")]
public readonly struct NumeralString16 : INumeralString
{
	private readonly ReadOnlyMemory<ushort> str;

	public int Radix { get; }

	public int Count => this.str.Length;

	public int this[int index] => this.str.Span[index];

	public ReadOnlyMemory<ushort> Values => this.str;

	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	internal string DebugDisplay => NumeralString.GetDebugString(this);

	private NumeralString16(int radix, ReadOnlyMemory<ushort> str, bool validate = true)
	{
		if (validate)
		{
			validateSpan(str.Span, radix);
		}

		this.Radix = radix;
		this.str = str;
	}

	public NumeralString16(int radix, ReadOnlyMemory<ushort> str)
		: this(radix, str, true)
	{
		if (radix <= 1)
			throw new ArgumentException("Radix must be greater than 1", nameof(radix));

		if (radix > 65536)
			throw new ArgumentException("Radix must be less than or equal to 65536", nameof(radix));
	}

	private static void validateSpan(ReadOnlySpan<ushort> values, int max)
	{
		for (int i = 0; i < values.Length; ++i)
		{
			if (values[i] >= max)
				throw new ArgumentException($"Invalid numeral string (Index {i} = {values[i]} >= {max})", nameof(values));
		}
	}

	public BigInteger ToBigInteger()
	{
		BigInteger res = BigInteger.Zero;
		for (int i = 0; i < this.Count; ++i)
		{
			res *= this.Radix;
			res += this.str.Span[i];
		}

		return res;
	}

	public static NumeralString16 FromBigInteger(BigInteger x, int radix, int m)
	{
		ushort[] str = new ushort[m];

		for (int i = 0; i < m; ++i)
		{
			str[m - 1 - i] = (ushort)(x % radix);
			x /= radix;
		}

		return new NumeralString16(radix, str);
	}

	public (NumeralString16, NumeralString16) Split(int pos)
	{
		return (
			new NumeralString16(this.Radix, this.str[..pos], false),
			new NumeralString16(this.Radix, this.str[pos..], false)
		);
	}

	(INumeralString, INumeralString) INumeralString.Split(int pos) => this.Split(pos);

	public IEnumerator<int> GetEnumerator()
	{
		for (int i = 0; i < this.str.Span.Length; i++)
		{
			ushort value = this.str.Span[i];
			yield return value;
		}
	}

	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}