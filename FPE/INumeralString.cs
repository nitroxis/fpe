using System.Numerics;

namespace FPE;

/// <summary>
/// Interface for numeral strings of a certain radix and length.
/// </summary>
public interface INumeralString : IReadOnlyList<int>
{
	/// <summary>
	/// Convert this numeral string to a <see cref="BigInteger"/>.
	/// </summary>
	/// <returns></returns>
	BigInteger ToBigInteger();

	/// <summary>
	/// Split the numeral string into two parts at the specified position.
	/// </summary>
	/// <param name="pos"></param>
	/// <returns></returns>
	(INumeralString, INumeralString) Split(int pos);
}